import 'package:flutter/material.dart';
import 'package:quiz_app/summary/question_individual_summary.dart';

class QuestionsSummary extends StatelessWidget {
  const QuestionsSummary({required this.summaryData, super.key});

  final List<Map<String, Object>> summaryData;

  @override
  Widget build(context) {
    return SizedBox(
      height: 550,
      child: SingleChildScrollView(
        child: Column(
          children: summaryData.map((item) {
            return QuestionIndividualSummary(item);
          }).toList(),
        ),
      ),
    );
  }
}
