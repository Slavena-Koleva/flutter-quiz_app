import 'package:flutter/material.dart';
import 'package:quiz_app/summary/question_identifier.dart';

class QuestionIndividualSummary extends StatelessWidget {
  const QuestionIndividualSummary(this.itemData, {super.key});

  final Map<String, Object> itemData;

  @override
  Widget build(context) {
    final bool isCorrect = itemData['answer'] == itemData['user_answer'];

    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
      QuestionIdentifier(itemData['question_index'] as int, isCorrect),
      Container(
        width: 10,
      ),
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              itemData['question'] as String,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
            const SizedBox(
              height: 3,
            ),
            Text(
              itemData['user_answer'] as String,
              style: TextStyle(
                color: isCorrect
                    ? Colors.blue
                    : const Color.fromARGB(255, 242, 27, 99),
                fontSize: 15,
              ),
            ),
            Text(itemData['answer'] as String,
                style: const TextStyle(
                  color: Colors.blue,
                  fontSize: 15,
                )),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      )
    ]);
  }
}
