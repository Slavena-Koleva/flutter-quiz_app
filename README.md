# Quiz app

My second flutter project. Covering 'advanced basics' like:

- if / else statements and ternary operators, comparison operators
- managing state with initState()
- passing data between components
- importing google fonts
- widgets such as Expanded, SingleChildScrollView

<p>
  <img src="public/images/startpage.png" width="200">
  <img src="public/images/questionpage.png" width="200">
  <img src="public/images/resultpage.png" width="200">
</p>

Features:

- answers are shuffled on each try
- they are stored and then can be seen in the results summary after the last question
- correct user answers are marked with blue, the correct answer can also be seen beneath each user answer
- the circles on the left side help to identify mistakes (marked in pink-red)
- click on restart quiz to start over
- the results are in scrollable view to cater for longer questionnaires
